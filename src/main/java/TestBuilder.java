import java.awt.EventQueue;

import javax.swing.JFrame;

public class TestBuilder {

    private JFrame mframe;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	EventQueue.invokeLater(new Runnable() {
	    public void run() {
		try {
		    TestBuilder window = new TestBuilder();
		    window.mframe.setVisible(true);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create the application.
     */
    public TestBuilder() {
	initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
	mframe = new JFrame();
	mframe.setBounds(100, 100, 450, 300);
	mframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
