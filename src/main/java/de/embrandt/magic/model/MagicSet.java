package de.embrandt.magic.model;

import java.util.List;

public class MagicSet{
	private String name;
	private String code;
	private String gathererCode;
	private String oldCode;
	private String releaseDate;
	private String border;
	private String type;
	private String block;
	private boolean onlineOnly;
	private List<String> booster;
	private Card[] cards;
	private int totalCards;
	private int gatheredCards;
	
	public String getBlock() {
		return block;
	}
	public List<String> getBooster() {
		return booster;
	}
	public String getBorder() {
		return border;
	}
	public Card[] getCards() {
		return cards;
	}
	public String getCode() {
		return code;
	}
	public String getGathererCode() {
		return gathererCode;
	}
	public String getName() {
		return name;
	}
	public String getOldCode() {
		return oldCode;
	}
	public String getReleaseDate() {
		return releaseDate;
	}


	public String getType() {
		return type;
	}

	public boolean isOnlineOnly() {
		return onlineOnly;
	}

	public void setBlock(String block) {
		this.block = block;
	}
	public void setBooster(List<String> booster) {
		this.booster = booster;
	}
	public void setBorder(String border) {
		this.border = border;
	}
	public void setCards(Card[] cards) {
		this.cards = cards;
		totalCards = cards.length;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setGathererCode(String gathererCode) {
		this.gathererCode = gathererCode;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setOnlineOnly(boolean onlineOnly) {
		this.onlineOnly = onlineOnly;
	}
	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public int getTotalCards() {
		return totalCards;
	}
	public int getGatheredCards() {
		return gatheredCards;
	}
	
	public void setTotalCards(int totalCards) {
		this.totalCards = totalCards;
	}
	public void setGatheredCards(int gatheredCards) {
		this.gatheredCards = gatheredCards;
	}
	@Override
	public String toString() {
		return "MagicSet [name=" + name + ", code=" + code + ", gathererCode="
				+ gathererCode + ", oldCode=" + oldCode + ", releaseDate="
				+ releaseDate + ", border=" + border + ", type=" + type
				+ ", block=" + block + ", onlineOnly=" + onlineOnly + "]";
	}
	
	public int getOnlineOnly() {
		return onlineOnly ? 1 : 0;
	}
	
	private String formatDatabaseString(String pString) {
		if (null == pString) {
			return "null";
		}
		String lConverted;
		lConverted = pString.replace("'", "''");
		lConverted = lConverted.replace("", "");
		return "'"+lConverted+"'";
	}
	
	public String toDatabaseValues() {
		return formatDatabaseString(name)+","+formatDatabaseString(code)+","+formatDatabaseString(gathererCode)+","+formatDatabaseString(oldCode)+","+formatDatabaseString(releaseDate)+","+formatDatabaseString(border)+","+formatDatabaseString(type)+","+formatDatabaseString(block)+","+getOnlineOnly()+",'"+booster+"'";
	}
}
