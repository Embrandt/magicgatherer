package de.embrandt.magic.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.text.NumberFormatter;

public class MagicSetTableModel extends AbstractTableModel {
	private List<MagicSet> mSets;
	private String[] mColumnName;

	public MagicSetTableModel() {
	    	mSets = new ArrayList<>();
		mColumnName = new String[] {"Name","Symbol","Anzahl Karten","Gesammelt","%"};
	}

	@Override
	public int getColumnCount() {
		return mColumnName.length;
	};

	@Override
	public String getColumnName(int column) {
		return mColumnName[column];
	}
	@Override
	public int getRowCount() {
		return mSets.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		MagicSet lRowSet = mSets.get(rowIndex);
		Object value = null;
		switch (columnIndex) {
		case 0:
			value = lRowSet.getName();
			break;
		case 1:
			value = lRowSet.getCode();
			break;
		case 2:
			value = lRowSet.getTotalCards();
			break;
		case 3:
			value = lRowSet.getGatheredCards();
			break;
		case 4:
			double percent = 0;
			if(lRowSet.getTotalCards() != 0) {
				percent = (double) lRowSet.getGatheredCards() / (double) lRowSet.getTotalCards()*100;
			}
			NumberFormat format = new DecimalFormat("#0.00");
			value = format.format(percent)+"%";
			break;
		}
		return value;
	}
	
	public List<MagicSet> getSets() {
		return mSets;
	}
	
	public void setSets(List<MagicSet> pSets) {
		mSets = pSets;
		fireTableDataChanged();
	}

}
