package de.embrandt.magic.model;

import java.util.List;

public class Card {
	private String id;
	private String layout;
	private String name;
	private List<String> names;
	private String manaCost;
	private double cmc;
	private List<String> colors;
	private String type;
	private List<String> supertypes;
	private List<String> types;
	private List<String> subtypes;
	private String rarity;
	private String text;
	private String flavor;
	private String artist;
	private String number;
	private String power;
	private String toughness;
	private int loyalty;
	private int multiverseid;
	private List<String> variations;
	private String imageName;
	private String watermark;
	private String border;
	private boolean timeshifted;
	private int hand;
	private int life;
	private boolean reserved;
	private String releaseDate;
	private String inSet;
	private ForeignName[] foreignNames;

	public void setForeignNames(ForeignName[] foreignNames) {
		this.foreignNames = foreignNames;
	}
	private int gathered;
	
	private String formatDatabaseString(boolean pBool) {
		return pBool ? "1" : "0";
	}
//	public ForeignName[] getForeignNames() {
//		return foreignNames;
//	}
	private String formatDatabaseString(String pString) {
		if (null == pString) {
			return "null";
		}
		String lConverted;
		lConverted = pString.replace("'", "''");
		lConverted = lConverted.replace("", "");
		return "'"+lConverted+"'";
	}
	
	public String getId() {
	    return id;
	}
	public void setId(String pId) {
	    id = pId;
	}
	public String getArtist() {
		return artist;
	}
	public String getBorder() {
		return border;
	}
	public double getCmc() {
		return cmc;
	}
	public List<String> getColors() {
		return colors;
	}
	public String getFlavor() {
		return flavor;
	}
	public int getGathered() {
		return gathered;
	}
	public int getHand() {
		return hand;
	}
	public String getImageName() {
		return imageName;
	}
	public String getLayout() {
		return layout;
	}
	public int getLife() {
		return life;
	}
	public int getLoyalty() {
		return loyalty;
	}
	public String getManaCost() {
		return manaCost;
	}
	public int getMultiverseid() {
		return multiverseid;
	}
	public String getName() {
		return name;
	}
	public List<String> getNames() {
		return names;
	}
	public String getNumber() {
		return number;
	}
	public String getPower() {
		return power;
	}
	public String getRarity() {
		return rarity;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public List<String> getSupertypes() {
		return supertypes;
	}
	public List<String> getSubtypes() {
		return subtypes;
	}
	public String getText() {
		return text;
	}
	public String getToughness() {
		return toughness;
	}
	public String getType() {
		return type;
	}
	public List<String> getTypes() {
		return types;
	}
	public List<String> getVariations() {
		return variations;
	}
	public String getWatermark() {
		return watermark;
	}
	public boolean isReserved() {
		return reserved;
	}
	public boolean isTimeshifted() {
		return timeshifted;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public void setBorder(String border) {
		this.border = border;
	}
	public void setCmc(double cmc) {
		this.cmc = cmc;
	}
	public void setColors(List<String> colors) {
		this.colors = colors;
	}
	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}
	public void setGathered(int gathered) {
		this.gathered = gathered;
	}
	public void setHand(int hand) {
		this.hand = hand;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}
	public void setLife(int life) {
		this.life = life;
	}
	public void setLoyalty(int loyalty) {
		this.loyalty = loyalty;
	}
	public void setManaCost(String manaCost) {
		this.manaCost = manaCost;
	}
	public void setMultiverseid(int multiverseid) {
		this.multiverseid = multiverseid;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setNames(List<String> names) {
		this.names = names;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public void setRarity(String rarity) {
		this.rarity = rarity;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}
	public void setSupertypes(List<String> supertypes) {
		this.supertypes = supertypes;
	}
	public void setSubtypes(List<String> suptypes) {
		this.subtypes = suptypes;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setTimeshifted(boolean timeshifted) {
		this.timeshifted = timeshifted;
	}
	public void setToughness(String toughness) {
		this.toughness = toughness;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setTypes(List<String> types) {
		this.types = types;
	}

	public void setVariations(List<String> variations) {
		this.variations = variations;
	}

	public void setWatermark(String watermark) {
		this.watermark = watermark;
	}
	public String toDatabaseValues() {

		return formatDatabaseString(id) + "," + formatDatabaseString(name) + "," + formatDatabaseString(names) + ",'" + layout + "'," + formatDatabaseString(manaCost) + ", " + cmc
				+ "," + formatDatabaseString(type) + ",'" + rarity + "'," + formatDatabaseString(text) + "," + formatDatabaseString(flavor)
				+ "," + formatDatabaseString(artist) + ",'" + number + "','" + power + "','"
				+ toughness + "', " + loyalty + " , " + multiverseid + ","
				+ formatDatabaseString(imageName) + ",'" + watermark + "','" + border + "',"
				+ formatDatabaseString(timeshifted) + ", " + hand + " , " + life + " ," + formatDatabaseString(reserved)
				+ ",'" + releaseDate + "'";
	}
	
	private String formatDatabaseString(List<String> pNames) {
	    if(pNames == null) {
		return "null";
	    }
	    String result = pNames.get(0);
	    pNames.remove(0);
	    for (String lName : pNames) {
		result = result + ";"+lName;
	    }
	    return formatDatabaseString(result);
	}
	public String toGatheredValues() {

		return formatDatabaseString(name) + ",'" + number + "'," + multiverseid +","+gathered;
	}
	public String getInSet() {
		return inSet;
	}
	public void setInSet(String inSet) {
		this.inSet = inSet;
	}
	
	public String getGermanName () {
		return getForeignName("German");
	}
	
	private String getForeignName (String pLanguage) {
		if (foreignNames == null) {
			return null;
		}
		for (ForeignName foreign : foreignNames) {
//			System.out.print(", "+foreign);
			if (pLanguage.equals(foreign.getLanguage())) {
				return foreign.getName();
			}
		}
		return null;
	}
	
	public String toTranslateDB() {
		return formatDatabaseString(name) + ","+formatDatabaseString(getGermanName());
	}
}
