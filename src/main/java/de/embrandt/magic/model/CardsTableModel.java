package de.embrandt.magic.model;

import java.awt.Robot;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.persistence.LockTimeoutException;
import javax.swing.table.AbstractTableModel;

import de.embrandt.magic.controller.DatabaseGatherer;
import de.embrandt.magic.view.CardControl;

public class CardsTableModel extends AbstractTableModel {
    public enum COLUMNNUMBER {
	NAME,COST,TYP,RARITY,NUMBER,GATHERED,ADDBUTTON,SUBBUTTON
    }
	private List<Card> mCards;
	private String[] mColumnName;

	public CardsTableModel() {
		mCards = new ArrayList<>();
		getLocalCardName(new Card());
		ResourceBundle lBundle = ResourceBundle.getBundle("BasicNames");
		mColumnName = new String[8];
		mColumnName[0] = lBundle.getString("name");//,lBundle.,"Kosten","Seltenheit","Anzahl"};
		mColumnName[1] = lBundle.getString("cost");
		mColumnName[2] = "Typ";
		mColumnName[3] = lBundle.getString("rarity");
		mColumnName[4] = "Sammlernummer";
		mColumnName[5] = "Anzahl";
		mColumnName[6] = "";
		mColumnName[7] = "";
	}

	@Override
	public int getColumnCount() {
		return mColumnName.length;
	};

	@Override
	public String getColumnName(int column) {
		return mColumnName[column];
	}
	@Override
	public int getRowCount() {
		return mCards.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Card lRowCard = mCards.get(rowIndex);
		Object value = null;
		switch (columnIndex) {
		case 0:
			value = getLocalCardName(lRowCard);
			break;
		case 1:
			value = lRowCard.getManaCost();
			break;
		case 2:
			value = lRowCard.getType();
			break;
		case 3:
			value = lRowCard.getRarity();
			break;
		case 4:
			value = lRowCard.getNumber();
			break;
		case 5:
			value = lRowCard.getGathered();
			break;
		case 6:
			value = "+";
			break;
		case 7:
			value = "-";
			break;
		}
		return value;
	}

	private String getLocalCardName(Card pCard) {
		ResourceBundle bundle = ResourceBundle.getBundle("CardResourceBundle", new CardControl());
		return bundle.getString(pCard.getName());
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if(columnIndex != 5) {
			return;
		}
		Card lRowCard = mCards.get(rowIndex);
		Integer value = (Integer) aValue;
		lRowCard.setGathered(Integer.valueOf(value));
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	public Card getCardAt(int rowIndex) {
		Card lRowCard = mCards.get(rowIndex);
		return lRowCard;
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 5 ) {
			return true;
		}
		if (columnIndex == 6 ) {
			return true;
		}
		if (columnIndex == 7 ) {
			return true;
		}
		return false;
	}
	
	public void setCards(List<Card> pCards) {
		mCards = pCards;
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
		case 1:
		case 2:
		case 3:
			return String.class;
		case 4:
		case 5:
			return Integer.class;
		default:
			break;
		}
		return super.getColumnClass(columnIndex);
	}
}
