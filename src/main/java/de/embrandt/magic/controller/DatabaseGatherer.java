package de.embrandt.magic.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import de.embrandt.magic.model.Card;
import de.embrandt.magic.model.MagicSet;

public class DatabaseGatherer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		getGatheredSet("FALSCH");
	}

	public static int getGatheredSet(String pSetCode) {
		String query = "SELECT * FROM gatheredcards WHERE INSET='"+pSetCode+"' AND COUNT>0;";
		List<Card> lAllCards = readCardsFromDatabase(query);
		return lAllCards.size();
	}
	
	public static void insertCardNumber (Card pCard) {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:gathered.db");
			connection.setAutoCommit(false);

			statement = connection.createStatement();
			String query = "REPLACE INTO gatheredcards (INSET,NAME,NUMBER,MULTIVERSEID,COUNT) VALUES ('"+pCard.getInSet()+"',"+pCard.toGatheredValues()+");";
			statement.executeUpdate(query);

			statement.close();
			connection.commit();
			connection.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}
	}
	public static int getAllGathered() {
		String query = "SELECT * FROM gatheredcards WHERE COUNT>0;";
		List<Card> lAllCards = readCardsFromDatabase(query);
		return lAllCards.size();
	}
	
	private static Connection createLocalConnection() throws SQLException {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		connection = DriverManager.getConnection("jdbc:sqlite:gathered.db");
		statement = connection.createStatement();
		String query = "CREATE TABLE if not exists gatheredcards ("+
					"NAME	TEXT NOT NULL,"+
					"INSET	TEXT NOT NULL,"+
					"NUMBER	TEXT,"+
					"MULTIVERSEID	INTEGER,"+
					"COUNT	INTEGER,"+
					"PRIMARY KEY(NAME,INSET,NUMBER,MULTIVERSEID));";
		statement.executeUpdate(query);
		statement.close();
		connection.setAutoCommit(false);
		return connection;
	}
	
	private static List<Card> readCardsFromDatabase(String pQuery) {
		Connection connection = null;
		Statement statement = null;
		ArrayList<Card> lCardList = new ArrayList<Card>();
		try {
			connection = createLocalConnection();

			statement = connection.createStatement();

			ResultSet res = statement.executeQuery(pQuery);
			Card lTempCard;
			while(res.next()) {
				lTempCard = new Card();
				lTempCard.setName(res.getString("NAME"));
				lTempCard.setNumber(res.getString("NUMBER"));
				lTempCard.setMultiverseid(res.getInt("MULTIVERSEID"));
				lTempCard.setGathered(res.getInt("COUNT"));
				lCardList.add(lTempCard);
			}
			res.close();
			statement.close();
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}
		return lCardList;
	}
}
