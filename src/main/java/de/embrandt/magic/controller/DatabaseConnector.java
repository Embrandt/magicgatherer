package de.embrandt.magic.controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import de.embrandt.magic.model.Card;
import de.embrandt.magic.model.ForeignName;
import de.embrandt.magic.model.MagicSet;

public class DatabaseConnector {

	private static Connection createInternalConnection() throws SQLException {
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String url = DatabaseConnector.class.getResource("/carddata/test.db").toString();
		connection = DriverManager.getConnection("jdbc:sqlite::resource:"+url);
		connection.setAutoCommit(false);
		return connection;
	}

	private static Connection createLocalConnection() throws SQLException {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		connection = DriverManager.getConnection("jdbc:sqlite:gathered.db");
		statement = connection.createStatement();
		String query = "CREATE TABLE if not exists gatheredcards ("+
					"NAME	TEXT NOT NULL,"+
					"INSET	TEXT NOT NULL,"+
					"NUMBER	TEXT,"+
					"MULTIVERSEID	INTEGER,"+
					"COUNT	INTEGER,"+
					"PRIMARY KEY(NAME,INSET,NUMBER,MULTIVERSEID));";
		statement.executeUpdate(query);
		statement.close();
		connection.setAutoCommit(false);
		return connection;
	}
	
	public static int getNumberOfCards(String pSetCode) {
		String query = "SELECT COUNT(*) FROM CARDS WHERE INSET ='"+pSetCode+"';";
		return readValueFromDatabase(query);
	}
	
	public static List<Card> getCardsWithName(String pName) {
		String query = "SELECT * FROM CARDS WHERE NAME LIKE '%"+pName+"%';";
		List<Card> lAllCards = readCardsFromDatabase(query);
		return lAllCards;
	}
	
	public static int getNumberOfCards() {
		String query = "SELECT COUNT(*) FROM CARDS;";
		return readValueFromDatabase(query);
	}
	
	public static void main(String[] args) {
		readAllSetsFromDatabase();
		System.out.println("Read cards "+getNumberOfCards("RTR"));
		System.out.println("Fake cards: "+readCardsFromDatabase("SELECT * FROM CARDS WHERE NAME='1';").size());
		List<Card> lLEA = readCardsFromSet("LEA");
		for (Card card : lLEA) {
			if ("Air Elemental".equals(card.getName())) {
				System.out.println(card.toDatabaseValues());
				System.out.println(card.getGathered());
			}
		}
		System.out.println("doofe nuss");
		ResourceBundle bundle = ResourceBundle.getBundle("Types", Locale.GERMAN);
		System.out.println("Bundle "+bundle.getString("Basic"));

	}
	
	public static List<Card> readCardsFromSet(String pSet) {
		String query = "SELECT * FROM CARDS WHERE INSET='"+pSet+"' ORDER BY RELEASEDATE;";
		List<Card> lAllCards = readCardsFromDatabase(query);
		for (Card card : lAllCards) {
			card.setInSet(pSet);
		}
		return lAllCards;
	}
	
	public static List<MagicSet> readAllSetsFromDatabase() {
		String query = "SELECT * FROM SETS ORDER BY RELEASEDATE;";
		List<MagicSet> lAllSets = readSetsFromDatabase(query);
		return lAllSets;
	}

	private static List<MagicSet> readSetsFromDatabase(String pQuery) {
		Connection connection = null;
		Statement statement = null;
		ArrayList<MagicSet> lSetList = new ArrayList<MagicSet>();
		try {
			connection = createInternalConnection();

			statement = connection.createStatement();

			ResultSet res = statement.executeQuery(pQuery);
			MagicSet lTempSet;
			while(res.next()) {
				lTempSet = new MagicSet();
				lTempSet.setCode(res.getString("CODE"));
				lTempSet.setName(res.getString("NAME"));
				lTempSet.setReleaseDate(res.getString("RELEASEDATE"));
				lTempSet.setBorder(res.getString("BORDER"));
				lTempSet.setGathererCode(res.getString("GATHERERCODE"));
				lTempSet.setType(res.getString("TYPE"));
				lTempSet.setBlock(res.getString("BLOCK"));
				lTempSet.setOldCode(res.getString("OLDCODE"));
				lTempSet.setOnlineOnly(res.getBoolean("ONLINEONLY"));
				lSetList.add(lTempSet);
			}
			res.close();
			statement.close();
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}
		return lSetList;
	}

	private static List<Card> readCardsFromDatabase(String pQuery) {
		Connection connection = null;
		Statement statement = null;
		ArrayList<Card> lCardList = new ArrayList<Card>();
		try {
			connection = createInternalConnection();

			statement = connection.createStatement();

			ResultSet res = statement.executeQuery(pQuery);
			Card lTempCard;
			while(res.next()) {
				lTempCard = new Card();
				lTempCard.setName(res.getString("NAME"));
				lTempCard.setInSet(res.getString("INSET"));
				lTempCard.setType(res.getString("TYPE"));
				lTempCard.setManaCost(res.getString("MANACOST"));
				lTempCard.setRarity(res.getString("RARITY"));
				lTempCard.setMultiverseid(res.getInt("MULTIVERSEID"));
				lTempCard.setNumber(res.getString("NUMBER"));
				lTempCard.setImageName(res.getString("IMAGENAME"));
				lTempCard.setText(res.getString("TEXT"));
				List<String> lNames = new ArrayList<String>();
				if (res.getString("NAMES") != null) {
				    for (String lPartName : res.getString("NAMES").split(";")) {
				        lNames.add(lPartName);
				    }
				}
				lTempCard.setNames(lNames);
//				ForeignName[] foreignNames = new ForeignName[1];
//				foreignNames[0] = new ForeignName();
//				foreignNames[0].setLanguage("German");
//				foreignNames[0].setName(res.getString("German"));
//				lTempCard.setForeignNames(foreignNames);
				Connection conn2 = createLocalConnection();
				Statement statm2 = conn2.createStatement();
				String query = "SELECT COUNT FROM gatheredcards WHERE NAME='"+lTempCard.getName().replace("'", "''")+"' AND MULTIVERSEID='"+lTempCard.getMultiverseid()+"' AND NUMBER='"+lTempCard.getNumber()+"';";
				ResultSet res2 = statm2.executeQuery(query);
				while(res2.next()) {
				lTempCard.setGathered(res2.getInt("COUNT"));
				}
				lCardList.add(lTempCard);
				res2.close();
				statm2.close();
				conn2.close();
			}
			res.close();
			statement.close();
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}
		return lCardList;
	}
	
	private static int readValueFromDatabase(String pQuery) {
		Connection connection = null;
		Statement statement = null;
		int value = -1;
		try {
			connection = createInternalConnection();

			statement = connection.createStatement();
			ResultSet res = statement.executeQuery(pQuery);
			value = res.getInt(1);
			res.close();

			statement.close();
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}
		return value;
	}
	
	public static List<MagicSet> readMainSets() {
		String query = "SELECT * FROM SETS WHERE TYPE='core' OR TYPE='expansion' ORDER BY RELEASEDATE;";
		List<MagicSet> lAllSets = readSetsFromDatabase(query);
		return lAllSets;
	}
	
	public static List<MagicSet> readSpecialSets() {
		String query = "SELECT * FROM SETS WHERE NOT(TYPE='core' OR TYPE='expansion') ORDER BY RELEASEDATE;";
		List<MagicSet> lAllSets = readSetsFromDatabase(query);
		return lAllSets;
	}
}
