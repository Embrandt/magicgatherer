package de.embrandt.magic.controller;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import de.embrandt.magic.model.Card;
import de.embrandt.magic.model.CardsTableModel;
import de.embrandt.magic.model.MagicSet;
import de.embrandt.magic.model.MagicSetTableModel;
import de.embrandt.magic.view.CardDetail;
import de.embrandt.magic.view.CardSearchDialog;

public class MagicSetController implements TableModelListener, ActionListener {

	private MagicSetTableModel mRegisteredModel;
	private CardsTableModel mCardsModel;
	private CardDetail mCardDetailView;
	private JPanel mMainPanel;
	private List<MagicSet> mMainSets;
	private List<MagicSet> mSpecialSets;
	
	public void setSetModel () {
		mMainSets = DatabaseConnector.readMainSets();
		mSpecialSets = DatabaseConnector.readSpecialSets();
		for (MagicSet lSet : mMainSets) {
			lSet.setTotalCards(DatabaseConnector.getNumberOfCards(lSet.getCode()));
			lSet.setGatheredCards(DatabaseGatherer.getGatheredSet(lSet.getCode()));
		}
		for (MagicSet lSet : mSpecialSets) {
			lSet.setTotalCards(DatabaseConnector.getNumberOfCards(lSet.getCode()));
			lSet.setGatheredCards(DatabaseGatherer.getGatheredSet(lSet.getCode()));
		}
		mRegisteredModel.setSets(mMainSets);
	}
	public String getSetName(String pSetCode) {
		List<MagicSet> lAllSets = new ArrayList<>();
		lAllSets.addAll(mMainSets);
		lAllSets.addAll(mSpecialSets);
		for (MagicSet lSet : lAllSets) {
		    if(lSet.getCode().equals(pSetCode)) {
			return lSet.getName();
		    }
		}
		return "";
	}
	public void setModel(MagicSetTableModel pModel) {
		mRegisteredModel = pModel;
	}
	
	public void setCardModel (CardsTableModel pModel) {
		mCardsModel = pModel;
	}
	
	public void setDetailView(CardDetail pDetailView) {
		mCardDetailView = pDetailView;
	}
	
	public void setMainPannel(JPanel pMainPannel) {
	    mMainPanel = pMainPannel;
	}
	
	public void changeCard(int pRow) {
		Card lChangedCard = mCardsModel.getCardAt(pRow);
		mCardDetailView.setCard(lChangedCard);		
	}
	
	@Override
	public void tableChanged(TableModelEvent e) {
		if (e.getColumn() == TableModelEvent.ALL_COLUMNS)
			return;
		if (e.getSource().equals(mCardsModel)) {
			int rowIndex = e.getFirstRow();
			Card lChangedCard = mCardsModel.getCardAt(rowIndex);
			DatabaseGatherer.insertCardNumber(lChangedCard);
			List<MagicSet> lAllSets = new ArrayList<>();
			lAllSets.addAll(mMainSets);
			lAllSets.addAll(mSpecialSets);
			for (MagicSet lSet : lAllSets) {
			    if(lSet.getCode().equals(lChangedCard.getInSet())) {
				lSet.setGatheredCards(DatabaseGatherer.getGatheredSet(lSet.getCode()));
				break;
			    }
			}
		} else {
			System.out.println("quelle ungleich receiver");
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.startsWith("+")) {
			int row = Integer.parseInt(command.substring(1));
			Card lCard = mCardsModel.getCardAt(row);
			int oldValue = lCard.getGathered();
			lCard.setGathered(oldValue+1);
			mCardsModel.fireTableCellUpdated(row, CardsTableModel.COLUMNNUMBER.GATHERED.ordinal());
		}
		if (command.startsWith("-")) {
			int row = Integer.parseInt(command.substring(1));
			Card lCard = mCardsModel.getCardAt(row);
			int oldValue = lCard.getGathered();
			if (oldValue > 0) {
				lCard.setGathered(oldValue-1);
				mCardsModel.fireTableCellUpdated(row, CardsTableModel.COLUMNNUMBER.GATHERED.ordinal());
			}
		}
	}
	
	public void setCards(String pSetCode) {
        List<Card> lCards = DatabaseConnector.readCardsFromSet(pSetCode);
        mCardsModel.setCards(lCards);
	}
	
	public void showSpecialEditions() {
		for (MagicSet lSet : mSpecialSets) {
			lSet.setGatheredCards(DatabaseGatherer.getGatheredSet(lSet.getCode()));
		}
		mRegisteredModel.setSets(mSpecialSets);
	}
	
	public void showMainEditions() {
		for (MagicSet lSet : mMainSets) {
			lSet.setGatheredCards(DatabaseGatherer.getGatheredSet(lSet.getCode()));
		}
		mRegisteredModel.setSets(mMainSets);
	}
	
	public void showDetails(int pRow) {
	    Card lChangedCard = mCardsModel.getCardAt(pRow);
	    mCardDetailView.setCard(lChangedCard);	
	    CardLayout cl = (CardLayout) mMainPanel.getLayout();
	    cl.next(mMainPanel);
	}

	public void search(CardSearchDialog pCardSearchDialog) {
	    // TODO Auto-generated method stub
	    System.out.println("Tada "+pCardSearchDialog.getSearchName());
	    List<Card> lCards = DatabaseConnector.getCardsWithName(pCardSearchDialog.getSearchName());
	    mCardsModel.setCards(lCards);
	    CardLayout cl = (CardLayout) mMainPanel.getLayout();
	    cl.next(mMainPanel);
	}
}
