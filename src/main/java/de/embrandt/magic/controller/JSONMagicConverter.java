package de.embrandt.magic.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import de.embrandt.magic.model.Card;
import de.embrandt.magic.model.MagicSet;
import flexjson.JSONDeserializer;

public class JSONMagicConverter {
	public static void main(String[] args) throws URISyntaxException {
		System.out.println("URL JSON"+JSONMagicConverter.class.getResource("/carddata/AllSets-x.json"));
		System.out.println("Converting JSON to database...");
		HashMap<String, MagicSet> lAllSets = JSONMagicConverter.readFromJSON();
//		downloadSymbols(lAllSets);
		JSONMagicConverter.writeToDatabase(lAllSets);
//		List<String> lAllTypes = new ArrayList<String>();
//		try {
//			FileWriter fw = new FileWriter(new File("CardTypes.properties"));
//			for(Entry<String, MagicSet> entry : lAllSets.entrySet()) {
//				System.out.print(entry.getKey()+", ");
//				//insertion of sets
//				MagicSet lSet = entry.getValue();
//				for (Card lCard : lSet.getCards()) {
//
//					List<String> supertypes = lCard.getSupertypes();
//					if (supertypes != null) {
//						for (String lString : supertypes) {
//							if(!lAllTypes.contains(lString)) {
//								fw.write(lString.toLowerCase()+" = "+lString+"\n");
//								lAllTypes.add(lString);
//							}
//						}
//					}
//
//					List<String> types = lCard.getTypes();
//					if (types != null) {
//						for (String lString : types) {
//							if(!lAllTypes.contains(lString)) {
//								fw.write(lString.toLowerCase()+" = "+lString+"\n");
//								lAllTypes.add(lString);
//							}
//						}
//					}
//
//					List<String> suptypes = lCard.getSubtypes();
//					if (suptypes != null) {
//						for (String lString : suptypes) {
//							if(!lAllTypes.contains(lString)) {
//								fw.write(lString.toLowerCase()+" = "+lString+"\n");
//								lAllTypes.add(lString);
//							}
//						}
//					}
//				}
//			}
//			fw.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		System.out.println("Converting finished.");
	}
	
	private static void downloadSymbols(HashMap<String, MagicSet> pSets) {
		URL website;
		HashMap<String, String> alternates = new HashMap<String, String>();
		alternates.put("VAN", "s");
		alternates.put("pDRC", null);
		alternates.put("RQS", null);
		alternates.put("pCEL", null);
		alternates.put("ITP", null);
		alternates.put("pPRE", null);
		alternates.put("pJGP", null);
		alternates.put("pALP", null);
		alternates.put("pWOS", null);
		alternates.put("pFNM", null);
		alternates.put("pELP", null);
		alternates.put("pMPR", null);
		alternates.put("pREL", null);
		alternates.put("pCMP", null);
		alternates.put("pGRU", null);
		alternates.put("pWPN", null);
		alternates.put("CST", null);
		alternates.put("TSB", "s");
		alternates.put("pPRO", null);
		alternates.put("pGPX", null);
		alternates.put("pMGD", null);
		alternates.put("pLPA", null);
		alternates.put("p15A", null);
		alternates.put("V14", "m");
		try {
			for (String key : pSets.keySet()) {
				String rarity = "c";
				if (alternates.containsKey(key)) {
					if (alternates.get(key) == null) {
						continue;
					} else {
						rarity = alternates.get(key);
					}
				}
			website = new URL("http://mtgimage.com/symbol/set/"+key+"/"+rarity+"/32.gif");
			if(key.equals("CON"))
				key = "_CON";
			File fos = new File("carddata/"+key+"/32.gif");
			org.apache.commons.io.FileUtils.copyURLToFile(website, fos);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static HashMap<String, MagicSet> readFromJSON() {
		HashMap<String, MagicSet> allSets = null;
		BufferedReader reader;
		try {
			//new FileReader("resources/carddata/AllSets-x.json")

			reader = new BufferedReader(new InputStreamReader(JSONMagicConverter.class.getResourceAsStream("/carddata/AllSets-x.json")));
			allSets = new JSONDeserializer<HashMap<String,MagicSet>>().use("values", MagicSet.class).deserialize(reader, HashMap.class);
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return allSets;
	}

	public static void writeToDatabase(HashMap<String, MagicSet> pAllSets) {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			String url = JSONMagicConverter.class.getResource("/carddata/test.db").toString();
			System.out.println("URL DB"+url);
			connection = DriverManager.getConnection("jdbc:sqlite::resource:"+url);
			connection.setAutoCommit(false);
			System.out.println("Opened database");

			statement = connection.createStatement();
			System.out.println("Write sets and cards to database");
			List<String> lTranslatedNames = new ArrayList<>();
			for(Entry<String, MagicSet> entry : pAllSets.entrySet()) {
				System.out.print(entry.getKey()+", ");
				//insertion of sets
				MagicSet lSet = entry.getValue();
				String query = "INSERT INTO SETS (NAME,CODE,GATHERERCODE,OLDCODE,RELEASEDATE,BORDER,TYPE,BLOCK,ONLINEONLY,BOOSTER) VALUES ("+lSet.toDatabaseValues()+");";
				try {
				statement.executeUpdate(query);
				} catch (SQLException e) {
					System.out.println();
					System.out.println("Set failed: "+lSet.toDatabaseValues());
					System.out.println("original data: "+lSet);
					throw e;
				}
				
				//insertion of single cards, using the code of the set as primary key
				Card[] lSetCards = lSet.getCards();
				Connection conn2 = DriverManager.getConnection("jdbc:sqlite:gathered.db");
				conn2.setAutoCommit(false);
				Statement stat2 = conn2.createStatement();
				for (Card lCard : lSetCards) {
					try {
						lCard.setInSet(entry.getKey());
						query = "INSERT INTO CARDS(INSET,ID,NAME,NAMES,LAYOUT,MANACOST,CMC,TYPE,RARITY,TEXT,FLAVOR,ARTIST,NUMBER,POWER,TOUGHNESS,LOYALTY,MULTIVERSEID,IMAGENAME,WATERMARK,BORDER,TIMESHIFTED,HAND,LIFE,RESERVED,RELEASEDATE) VALUES ('"+entry.getKey()+"',"+lCard.toDatabaseValues()+");";
						statement.executeUpdate(query);
						if(lCard.getGermanName() != null || !lTranslatedNames.contains(lCard.getName())) {
						    query = "REPLACE INTO CARDNAMES(NAME,German) VALUES ("+lCard.toTranslateDB()+");";
						    statement.executeUpdate(query);
						    lTranslatedNames.add(lCard.getName());
						}
					} catch (SQLException e) {
						System.out.println("Card failed: "+lCard.toDatabaseValues());
						System.out.println("original data: "+lCard);
						throw e;
					}
				}
				stat2.close();
				conn2.commit();
				conn2.close();
			}
			System.out.println("DONE");
			System.out.println("Wrote data to database");
			statement.close();
			connection.commit();
			connection.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}
	}
}
