package de.embrandt.magic.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.net.URL;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import sun.swing.DefaultLookup;

public class ManacostRenderer extends JLabel implements TableCellRenderer {

	public ManacostRenderer() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		setOpaque(true);
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		String lManacost = (String) value;
		removeAll();

		if (table == null) {
			return this;
		}

		if (isSelected) {
			super.setForeground(table.getSelectionForeground());
			super.setBackground(table.getSelectionBackground());
		} else {
			Color background = table.getBackground();
			super.setForeground(table.getForeground());
			super.setBackground(table.getForeground());
		}
		

		if (value == null) {
			this.add(new JLabel(""));
			return this;
		}

		String [] lPreparedCost = prepareManaCost(lManacost.toLowerCase());
		for (String cost : lPreparedCost) {
			JLabel lIcon = new JLabel();
			cost = cost.replace("/", "");
			URL lIconUrl = SetIconRenderer.class.getResource("/carddata/symbol/mana/"+cost+"/32.gif");
			if (lIconUrl == null) {
				System.err.println("kann mana nicht finden");
				continue;
			}
			ImageIcon lImage = new ImageIcon(lIconUrl);
			lImage.setImage(lImage.getImage().getScaledInstance(16,16,Image.SCALE_DEFAULT));
			lIcon.setIcon(lImage);

			this.add(lIcon);
		}
		return this;
	}
	
	private String[] prepareManaCost (String pManacost) {
		return pManacost.replace("{", "").split("}");
	}
}
