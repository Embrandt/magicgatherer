package de.embrandt.magic.view;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import de.embrandt.magic.controller.DatabaseConnector;
import de.embrandt.magic.model.Card;
import de.embrandt.magic.model.ForeignName;

public class CardControl extends Control {

	@Override
	public ResourceBundle newBundle(String baseName, Locale locale,
			String format, ClassLoader loader, boolean reload)
			throws IllegalAccessException, InstantiationException, IOException {
		// TODO Auto-generated method stub
		return new CardResourceBundle();
	}

}

class CardResourceBundle extends ResourceBundle {


	@Override
	protected Object handleGetObject(String key) {
		if (key == null) {
			return "";
		}
		String pQuery = "SELECT * FROM cardnames WHERE NAME='"+key.replace("'", "''")+"';";
		Statement statement = null;
		String localName = null;
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			String url = DatabaseConnector.class.getResource("/carddata/test.db").toString();
			connection = DriverManager.getConnection("jdbc:sqlite::resource:"+url);
			statement = connection.createStatement();

			ResultSet res = statement.executeQuery(pQuery);

			if(res.next()) {
				try {
					localName = res.getString(getLocale().getDisplayLanguage(Locale.US));
					if (localName == null) {
						localName = res.getString("NAME");
					}
				} catch(SQLException e) {
					localName = res.getString("NAME");
				}
			} else {
				localName = null;
			}
			res.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}

		return localName;
	}

	@Override
	public Enumeration<String> getKeys() {
		String pQuery = "SELECT * FROM cardnames;";
		Statement statement = null;
		Connection connection = null;
		List<String> localNames = new ArrayList<String>();
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			String url = DatabaseConnector.class.getResource("/carddata/test.db").toString();
			connection = DriverManager.getConnection("jdbc:sqlite::resource:"+url);
			statement = connection.createStatement();

			ResultSet res = statement.executeQuery(pQuery);

			while(res.next()) {
				String localName = res.getString(getLocale().getLanguage());
				if (localName == null) {
					localName = res.getString("NAME");
				}
				localNames.add(localName);
			}
			res.close();
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQL Exception "+e.getClass()+" : "+e.getMessage());
			e.printStackTrace();
		}

		return Collections.enumeration(localNames);
	}

}
