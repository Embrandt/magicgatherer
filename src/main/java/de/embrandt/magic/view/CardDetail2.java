package de.embrandt.magic.view;

import java.awt.EventQueue;
import java.awt.Image;
import java.net.URL;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.embrandt.magic.controller.MagicSetController;

import java.awt.BorderLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout.Group;

public class CardDetail2 extends JDialog {
    private javax.swing.JLabel cardImage;
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CardDetail2 dialog = new CardDetail2();
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the dialog.
     */
    public CardDetail2() {
        //setBounds(100, 100, 450, 300);
        cardImage = new javax.swing.JLabel();
        cardImage.setBackground(new java.awt.Color(0, 0, 0));
        URL lIconUrl = SetIconRenderer.class.getResource("/carddata/image/cardback.jpg");
        if (lIconUrl == null) {
            System.err.println("kann image nicht finden");
        }
        ImageIcon lImage = new ImageIcon(lIconUrl);
        lImage.setImage(lImage.getImage().getScaledInstance(336,476,Image.SCALE_DEFAULT));
        cardImage.setIcon(lImage);
        //cardImage.setText("Cardimage");
        //cardImage.setMaximumSize(new java.awt.Dimension(48, 68));
        //cardImage.setMinimumSize(new java.awt.Dimension(48, 68));
        cardImage.setPreferredSize(new java.awt.Dimension(336, 476));
        cardImage.setRequestFocusEnabled(false);
        JLabel multiverseid_label = new JLabel("MultiverseID");
        
        GroupLayout layout = new GroupLayout(getContentPane());
        Group titleGroup = layout.createSequentialGroup().addComponent(multiverseid_label);
        Group mainGroup = layout.createParallelGroup();
        Group bottomGroup = layout.createSequentialGroup();

        layout.setHorizontalGroup(
            layout.createSequentialGroup()
                    .addComponent(cardImage)
                    .addGroup(layout.createParallelGroup().addGroup(titleGroup).addGroup(mainGroup).addGroup(bottomGroup))
        );

        layout.setVerticalGroup(
            layout.createParallelGroup(Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(Alignment.LEADING)
                        .addComponent(cardImage)
                        .addComponent(multiverseid_label)))
        );
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        getContentPane().setLayout(layout);
        pack();
        //JPanel panel = new CardDetail(new MagicSetController());
        //getContentPane().add(panel, BorderLayout.CENTER);

    }

}
