package de.embrandt.magic.view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class SearchDialog extends JDialog
{
  private JPanel hauptPanel = new JPanel();
  private JTextField suchTextFeld = new JTextField();
  private JRadioButton yesButton  = new JRadioButton("Beachten");
  private JRadioButton noButton   = new JRadioButton("Nicht beachten");
  private JButton searchButton    = new JButton("Suchen");
  private JButton cancelButton    = new JButton("Abbrechen");
  //private InfoTransferObject ITO;
  private ButtonGroup gruppe = new ButtonGroup();

  public SearchDialog(Frame frame)
  {
    super(frame,"Suchdialog",true);

    JLabel aufforderung = new JLabel("Suchwort:");
    JLabel gkFrage      = new JLabel("Groß/klein beachten?");
    JLabel fueller      = new JLabel();

    this.getContentPane().setLayout(new BorderLayout());
    this.setLocation(300,300);
    this.getContentPane().add(hauptPanel,"Center");


    hauptPanel.setLayout(new GridLayout(4,2,10,0));
    hauptPanel.add(aufforderung);
    hauptPanel.add(gkFrage);
    hauptPanel.add(suchTextFeld);
    hauptPanel.add(yesButton);
    hauptPanel.add(fueller);
    hauptPanel.add(noButton);
    hauptPanel.add(searchButton);
    hauptPanel.add(cancelButton);


    gruppe.add(yesButton);
    gruppe.add(noButton);
    yesButton.setActionCommand("beacht");
    noButton.setActionCommand("nicht");
    yesButton.setSelected(true);
    noButton.setSelected(false);

    searchListener SLis = new searchListener();
    searchButton.addActionListener(SLis);
    cancelButton.addActionListener(SLis);



    this.pack();
  }

  public void showIt()
  {
    //ITO = i;
    setVisible(true);
  }

  class searchListener implements ActionListener
  {
   public void actionPerformed(ActionEvent evt)
     {
       String suchText = suchTextFeld.getText();
       boolean grkl = (gruppe.getSelection().getActionCommand().equals("beacht"));
       String command = evt.getActionCommand();
       if(command.equals("Abbrechen"))
       {
        //ITO.setAll(suchText,grkl,false);
        setVisible(false);
       }
       else if (command.equals("Suchen"))
       {
        //ITO.setAll(suchText,grkl,true);
        setVisible(false);
       }
     }

  }
}

