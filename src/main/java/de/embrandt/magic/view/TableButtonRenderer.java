package de.embrandt.magic.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class TableButtonRenderer extends AbstractCellEditor implements TableCellRenderer, TableCellEditor,ActionListener {

	private JButton button1;
	private JButton button2;

	public TableButtonRenderer(String action) {
		 button1 = new JButton(action);
		 button2 = new JButton(action);
		 button2.addActionListener(this);
	}
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		if (table == null) {
			return button1;
		}
		if (isSelected) {
			button1.setForeground(table.getSelectionForeground());
			button1.setBackground(table.getSelectionBackground());
		} else {
			Color background = table.getBackground();
			button1.setForeground(table.getForeground());
			button1.setBackground(background);
		}
		return button1;
	}

	@Override
	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
			boolean isSelected, int row, int column) {
		if (table == null) {
			return button2;
		}
		if (isSelected) {
			button2.setForeground(table.getSelectionForeground());
			button2.setBackground(table.getSelectionBackground());
		} else {
			Color background = table.getBackground();
			button2.setForeground(table.getForeground());
			button2.setBackground(background);
		}
		button2.setActionCommand(button2.getText()+table.convertRowIndexToModel(row));
		return button2;
	}
	
	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		// TODO Auto-generated method stub
		return false;
	}
	public JButton getButton() {
		return button2;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		fireEditingStopped();
	}

}
