package de.embrandt.magic.view;

import java.awt.ComponentOrientation;
import java.awt.Image;
import java.awt.Label;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class ManaCostLabel extends JLabel {
	
	public ManaCostLabel(String pManaCost) {
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		setOpaque(true);

	}
	
	public void changeCost(String pManaCost) {
		removeAll();
		if (pManaCost == null) {
			this.add(new JLabel(""));
			return;
		}
		System.out.println("Set manacost: "+pManaCost);
		String [] lPreparedCost = prepareManaCost(pManaCost.toLowerCase());
		for (int i=lPreparedCost.length-1;i>=0;i--) {
			String cost = lPreparedCost[i];
			JLabel lIcon = new JLabel();
			cost = cost.replace("/", "");
			String lurlString = "/carddata/symbol/mana/"+cost+"/32.gif";
			URL lIconUrl = SetIconRenderer.class.getResource(lurlString);
			if (lIconUrl == null) {
				System.err.println("kann mana nicht finden "+lurlString);
				continue;
			}
			ImageIcon lImage = new ImageIcon(lIconUrl);
			lImage.setImage(lImage.getImage().getScaledInstance(16,16,Image.SCALE_DEFAULT));
			lIcon.setIcon(lImage);
			this.add(lIcon);
		}
		


	}
	
	private String[] prepareManaCost (String pManacost) {
		return pManacost.replace("{", "").split("}");
	}
}
