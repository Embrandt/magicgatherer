package de.embrandt.magic.view;

import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableCellRenderer;

import de.embrandt.magic.controller.JSONMagicConverter;
import de.embrandt.magic.model.MagicSet;

public class SetIconRenderer extends DefaultTableCellRenderer {

	public SetIconRenderer() {
		setHorizontalAlignment(CENTER);
	}
	@Override
	protected void setValue(Object value) {
		ImageIcon lSetIcon = null;
		String lSetCode = (String) value;
		URL lIconUrl = SetIconRenderer.class.getResource("/carddata/symbol/set/"+lSetCode+"/64.gif");

//		if (lIconUrl == null) {
//			String  lImageLocation = "http://mtgimage.com/symbol/set/"+lSetCode+"/c/32.gif";
//			try {
//			lIconUrl = new URL(lImageLocation);
//			} catch (MalformedURLException e) {
//				e.printStackTrace();
//			}
//		}
		if (lIconUrl == null) {
			setIcon(null);
			return;
		}

		lSetIcon = new ImageIcon(lIconUrl);
		setIcon(lSetIcon);
	}
}
