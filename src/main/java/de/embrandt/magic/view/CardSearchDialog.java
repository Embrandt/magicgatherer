package de.embrandt.magic.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import de.embrandt.magic.controller.MagicSetController;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CardSearchDialog extends JDialog {

    private final JPanel mcontentPanel = new JPanel();
    private JTextField mNameSearchField;
    private JLabel mblName;
    private MagicSetController mController;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
	try {
	    CardSearchDialog dialog = new CardSearchDialog(new MagicSetController());
	    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	    dialog.setVisible(true);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Create the dialog.
     */
    public CardSearchDialog(MagicSetController pController) {
	mController = pController;
	setBounds(100, 100, 450, 300);
	getContentPane().setLayout(new BorderLayout());
	mcontentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(mcontentPanel, BorderLayout.CENTER);
	{
		mblName = new JLabel("Name");
	}
	{
		mNameSearchField = new JTextField();
		mNameSearchField.setColumns(10);
	}
	GroupLayout gl_contentPanel = new GroupLayout(mcontentPanel);
	gl_contentPanel.setHorizontalGroup(
		gl_contentPanel.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_contentPanel.createSequentialGroup()
				.addContainerGap()
				.addComponent(mblName)
				.addGap(54)
				.addComponent(mNameSearchField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGap(247))
	);
	gl_contentPanel.setVerticalGroup(
		gl_contentPanel.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_contentPanel.createSequentialGroup()
				.addGap(8)
				.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
					.addComponent(mblName)
					.addComponent(mNameSearchField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(190, Short.MAX_VALUE))
	);
	mcontentPanel.setLayout(gl_contentPanel);
	{
	    JPanel buttonPane = new JPanel();
	    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    {
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    search();
			}
		});
		okButton.setActionCommand("search");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	    }
	    {
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    setVisible(false);
			}
		});
		cancelButton.setActionCommand("cancel");
		buttonPane.add(cancelButton);
	    }
	}
    }
    
    private void search() {
	mController.search(this);
	setVisible(false);
    }
    public String getSearchName() {
	return mNameSearchField.getText();
    }
}
